package com.afrozaar.nimbal.rest;

import com.afrozaar.nimbal.api.ErrorLoadingArtifactException;
import com.afrozaar.nimbal.api.INimbalService;
import com.afrozaar.nimbal.api.ModuleLoadException;
import com.afrozaar.nimbal.core.MavenCoords;
import com.afrozaar.nimbal.core.Module;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.Map;

@RestController
@RequestMapping("modules")
public class ModuleManagerController {

    private static final org.slf4j.Logger LOG = org.slf4j.LoggerFactory.getLogger(ModuleManagerController.class);

    private INimbalService facade;

    public ModuleManagerController(INimbalService  facade) {
        super();
        this.facade = facade;
    }

    @GetMapping
    public Map<String, Module> getModules() {
        return facade.getModules();
    }

    @GetMapping("/{module}")
    public Object getModule(@PathVariable("module") String module) {
        return facade.getModule(module);
    }

    @SuppressWarnings("unchecked")
    @GetMapping("/load/{groupId}:{artifactId}:{version}")
    public Module loadModule(@PathVariable("groupId") String groupId, @PathVariable("artifactId") String artifactId, @PathVariable("version") String version)
            throws MalformedURLException, ClassNotFoundException, ErrorLoadingArtifactException, ModuleLoadException, IOException {
        return facade.loadModule(new MavenCoords(groupId, artifactId, version));
    }

    @GetMapping("/unload/{moduleName}")
    public void unloadModule(@PathVariable("moduleName") String moduleName) throws ModuleLoadException {
        facade.unloadModule(moduleName);
    }

}
