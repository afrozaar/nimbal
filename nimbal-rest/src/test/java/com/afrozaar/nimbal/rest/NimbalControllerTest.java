package com.afrozaar.nimbal.rest;

import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(SpringRunner.class)
@SpringBootTest
public class NimbalControllerTest {

    private static final org.slf4j.Logger LOG = org.slf4j.LoggerFactory.getLogger(NimbalControllerTest.class);

    @Test
    public void contextLoads() throws Exception {}

}
