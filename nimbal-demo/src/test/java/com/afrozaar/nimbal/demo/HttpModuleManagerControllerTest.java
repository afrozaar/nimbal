package com.afrozaar.nimbal.demo;

import static org.assertj.core.api.Assertions.assertThat;

import com.google.common.collect.ImmutableMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.junit4.SpringRunner;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.util.Map;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class HttpModuleManagerControllerTest {

    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

    private static final org.slf4j.Logger LOG = org.slf4j.LoggerFactory.getLogger(HttpModuleManagerControllerTest.class);

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    private String base;

    @Before
    public void setup() {
        base = "http://localhost:" + port + "/modules";
    }

    @Test
    public void Load() throws JsonParseException, JsonMappingException, IOException {
        {
            String module = this.restTemplate.getForObject(base + "/load/{groupId}:{artifactId}:{version}",
                    String.class, ImmutableMap.of("groupId", "com.afrozaar.nimbal.test", "artifactId", "nimbal-test-simple", "version", "1.0.0-SNAPSHOT"));
            //assertThat(module.getName()).isEqualTo("DefaultConfiguration");
            JsonNode readValue = OBJECT_MAPPER.readTree(module);
            assertThat(readValue.get("name").asText()).isEqualTo("DefaultConfiguration");
        }

        {
            String forObject = restTemplate.getForObject(base, String.class);
            Map<String, JsonNode> readValue = OBJECT_MAPPER.readValue(forObject, new TypeReference<Map<String, JsonNode>>() {});
            assertThat(readValue).containsKey("DefaultConfiguration");
        }
    }
}
