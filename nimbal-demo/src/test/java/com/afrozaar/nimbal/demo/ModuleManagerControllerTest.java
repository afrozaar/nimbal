package com.afrozaar.nimbal.demo;

import static org.assertj.core.api.Assertions.assertThat;

import com.afrozaar.nimbal.core.Module;
import com.afrozaar.nimbal.rest.ModuleManagerController;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Map;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ModuleManagerControllerTest {

    private static final org.slf4j.Logger LOG = org.slf4j.LoggerFactory.getLogger(ModuleManagerControllerTest.class);

    @Autowired
    private ModuleManagerController controller;

    @Test
    public void contexLoads() throws Exception {
        assertThat(controller).isNotNull();

        Map<String, Module> modules = controller.getModules();
        assertThat(modules).isNotNull().isEmpty();

        Module loadModule = controller.loadModule("com.afrozaar.nimbal.test", "nimbal-test-simple", "1.0.0-SNAPSHOT");

        assertThat(loadModule.getName()).isEqualTo("DefaultConfiguration");
        assertThat(controller.getModules()).containsKey("DefaultConfiguration");

        assertThat(controller.getModule("DefaultConfiguration")).isNotNull();

        controller.unloadModule("DefaultConfiguration");

        assertThat(controller.getModules()).isEmpty();
    }
}
