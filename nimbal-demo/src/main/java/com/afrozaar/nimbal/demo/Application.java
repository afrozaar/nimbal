package com.afrozaar.nimbal.demo;

import com.afrozaar.nimbal.api.INimbalService;
import com.afrozaar.nimbal.core.ContextLoader;
import com.afrozaar.nimbal.core.NimbalService;
import com.afrozaar.nimbal.rest.ModuleManagerController;

import org.springframework.beans.BeansException;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class Application implements ApplicationContextAware {

    private static final org.slf4j.Logger LOG = org.slf4j.LoggerFactory.getLogger(Application.class);

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    private ApplicationContext context;

    @Bean
    public INimbalService getContextLoader() {
        return new NimbalService(new ContextLoader.Builder().setParentContext(context).build());
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.context = applicationContext;
    }

    @Bean
    public ModuleManagerController moduleManagerController() {
        return new ModuleManagerController(getContextLoader());
    }
}
