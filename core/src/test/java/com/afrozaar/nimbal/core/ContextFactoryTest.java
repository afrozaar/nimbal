/*******************************************************************************
 * Nimbal Module Manager 
 * Copyright (c) 2017 Afrozaar.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Apache License 2.0
 * which accompanies this distribution and is available at https://www.apache.org/licenses/LICENSE-2.0
 *******************************************************************************/
package com.afrozaar.nimbal.core;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.failBecauseExceptionWasNotThrown;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import com.afrozaar.nimbal.api.ErrorLoadingArtifactException;
import com.afrozaar.nimbal.core.ContextFactory.ParentContext;
import com.afrozaar.nimbal.core.classloader.ClassLoaderFactory;

import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class ContextFactoryTest {

    @SuppressWarnings("unused")
    private static final org.slf4j.Logger LOG = org.slf4j.LoggerFactory.getLogger(ContextFactoryTest.class);

    @Spy
    private IRegistry registry = new SimpleRegistry();
    @Spy
    private ClassLoaderFactory classLoaderFactory = new ClassLoaderFactory(registry);

    @InjectMocks
    private ContextFactory contextFactory /*= new ContextFactory(classLoaderFactory, registry)*/;

    @Mock
    private ApplicationContext applicationContext;

    @Test
    public void DefaultParentContext() throws ErrorLoadingArtifactException {
        contextFactory.setApplicationContext(applicationContext);
        ParentContext parentContext = contextFactory.getParentContext(new ModuleInfo());

        assertThat(parentContext.getApplicationContext()).isSameAs(applicationContext);
        assertThat(parentContext.getClassLoader()).isSameAs(this.getClass().getClassLoader());
    }

    @Test
    public void CheckErrorWhenParentNotPresent() {
        contextFactory.setApplicationContext(applicationContext);
        ModuleInfo module = new ModuleInfo();
        module.setName("bar");
        module.setParentModule("foo");
        try {
            contextFactory.getParentContext(module);
            failBecauseExceptionWasNotThrown(ErrorLoadingArtifactException.class);
        } catch (ErrorLoadingArtifactException e) {
            assertThat(e.getMessage()).isEqualTo("module bar required parent module foo but is not present.");
        }
    }

    @Test
    public void WhenParentContext() throws ErrorLoadingArtifactException {
        contextFactory.setApplicationContext(applicationContext);

        String parentModule = "foo";

        ConfigurableApplicationContext mockApplicationContext = mock(ConfigurableApplicationContext.class);
        ClassLoader mockClassLoader = mock(ClassLoader.class);
        { // parent module
            ModuleInfo moduleInfo = new ModuleInfo();
            moduleInfo.setName(parentModule);
            when(registry.getModule(parentModule)).thenReturn(new Module(moduleInfo, mockClassLoader, mockApplicationContext));
        }

        // child module
        ModuleInfo module = new ModuleInfo();
        module.setName("bar");
        module.setParentModule(parentModule);

        ParentContext parentContext = contextFactory.getParentContext(module);

        assertThat(parentContext.getApplicationContext()).isSameAs(mockApplicationContext);
        assertThat(parentContext.getClassLoader()).isSameAs(mockClassLoader);

    }

    @Test
    public void WhenParentClassLoaderOnly() throws ErrorLoadingArtifactException {
        contextFactory.setApplicationContext(applicationContext);

        ModuleInfo module = new ModuleInfo();
        module.setName("bar");
        String parentModule = "foo";
        module.setParentClassLoaderOnly(true);
        module.setParentModule(parentModule);

        ConfigurableApplicationContext mockApplicationContext = mock(ConfigurableApplicationContext.class);
        ClassLoader mockClassLoader = mock(ClassLoader.class);

        when(registry.getModule(parentModule)).thenReturn(new Module(new ModuleInfo("bar"), mockClassLoader, mockApplicationContext));

        ParentContext parentContext = contextFactory.getParentContext(module);

        assertThat(parentContext.getApplicationContext()).isSameAs(applicationContext);
        assertThat(parentContext.getClassLoader()).isSameAs(mockClassLoader);
    }

}
