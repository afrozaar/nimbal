/*******************************************************************************
 * Nimbal Module Manager 
 * Copyright (c) 2017 Afrozaar.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Apache License 2.0
 * which accompanies this distribution and is available at https://www.apache.org/licenses/LICENSE-2.0
 *******************************************************************************/
package com.afrozaar.nimbal.core;

import static org.assertj.core.api.Assertions.assertThat;
import static org.joor.Reflect.on;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.afrozaar.nimbal.api.ErrorLoadingArtifactException;
import com.afrozaar.nimbal.api.ModuleLoadException;
import com.afrozaar.nimbal.core.classloader.ClassLoaderFactory;
import com.afrozaar.nimbal.core.classloader.URLClassLoaderExtension;

import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentMatchers;

import java.util.function.Supplier;

import java.io.IOException;
import java.net.MalformedURLException;

public class OrchestrationTest {

    @SuppressWarnings("unused")
    private static final org.slf4j.Logger LOG = org.slf4j.LoggerFactory.getLogger(OrchestrationTest.class);
    private ApplicationContext testApplicationContext;
    private ContextLoader loader;

    SimpleRegistry registry = new SimpleRegistry();

    @Before
    public void setupTestAppContext() {
        this.testApplicationContext = new AnnotationConfigApplicationContext(TestConfiguration.class);
        MavenRepositoriesManager manager = setupDefaultMavenRepo();

        ClassLoaderFactory factory = new ClassLoaderFactory(registry);
        ContextFactory contextFactory = new ContextFactory(factory, registry);
        contextFactory.setApplicationContext(testApplicationContext);
        loader = new ContextLoader(manager, factory, registry, contextFactory);

    }

    @SuppressWarnings("unchecked")
    @Test
    public void LoadSimpleContext() throws ErrorLoadingArtifactException, MalformedURLException, IOException, ClassNotFoundException, ModuleLoadException {

        ApplicationContext context = loader.loadModule(new MavenCoords("com.afrozaar.nimbal.test", "nimbal-test-simple", "1.0.0-SNAPSHOT")).getContext();

        //LOG.info("beans now present {}", context.getBeanDefinitionNames());
        Object bean = context.getBean("getBean");

        assertThat(bean).isNotNull();
        String x = ((Supplier<String>) bean).get();
        assertThat(x).isEqualTo("from test simple");
        assertThat(bean.getClass().getName()).isEqualTo("com.afrozaar.nimbal.test.SpringManagedObject");
    }

    @SuppressWarnings("unchecked")
    @Test
    public void LoadConfirmBeforeAndAfterInvoked() throws ErrorLoadingArtifactException, MalformedURLException, IOException, ClassNotFoundException,
            ModuleLoadException {

        ApplicationContext context = loader.loadModule(new MavenCoords("com.afrozaar.nimbal.test", "nimbal-test-module-annotation", "1.0.0-SNAPSHOT"))
                .getContext();

        //LOG.info("beans now present {}", context.getBeanDefinitionNames());
        Object bean = context.getBean("defaultConfiguration");

        String beforeRefresh = on(bean).call("getBeforeRefresh").get();
        assertThat(beforeRefresh).isEqualTo("yes");
        String afterRefresh = on(bean).call("getAfterRefresh").get();
        assertThat(afterRefresh).isEqualTo("yes");
    }

    @Test
    @SuppressWarnings("unchecked")
    public void LoadComplexContext() throws ErrorLoadingArtifactException, MalformedURLException, IOException, ClassNotFoundException, ModuleLoadException {

        ApplicationContext context = loader.loadModule(new MavenCoords("com.afrozaar.nimbal.test", "nimbal-test-module-complex-annotation", "1.0.0-SNAPSHOT"))
                .getContext();

        //LOG.info("beans now present {}", context.getBeanDefinitionNames());
        Object bean = context.getBean("getBean");

        assertThat(bean).isNotNull();
        String x = ((Supplier<String>) bean).get();
        assertThat(x).isEqualTo("from test simple");
        assertThat(bean.getClass().getName()).isEqualTo("com.afrozaar.nimbal.test.SpringManagedObject");

        URLClassLoaderExtension extensionClassLoader = (URLClassLoaderExtension) bean.getClass().getClassLoader();

        assertThat(extensionClassLoader.getRingFencedFilters()).containsOnly("foo", "bar");
        assertThat(extensionClassLoader.getName()).isEqualTo("ComplexModule");
    }

    private MavenRepositoriesManager setupDefaultMavenRepo() {
        MavenRepositoriesManager manager = new MavenRepositoriesManager("http://repo1.maven.org/maven2", true);
        manager.setM2Folder(".m2");
        manager.init();
        return manager;
    }

    @SuppressWarnings("unchecked")
    @Test
    public void LoadAndReloadWithChange() throws ErrorLoadingArtifactException, MalformedURLException, IOException, ClassNotFoundException,
            ModuleLoadException {

        String name = loader.loadModule(new MavenCoords("com.afrozaar.nimbal.test", "nimbal-test-module-annotation", "1.0.0-SNAPSHOT")).getName();
        {
            ApplicationContext context = registry.getModule(name).getContext();

            //LOG.info("beans now present {}", context.getBeanDefinitionNames());
            Object bean = context.getBean("getBean");

            assertThat(bean).isNotNull();
            String x = ((Supplier<String>) bean).get();
            assertThat(x).isEqualTo("from test simple 1");

            assertThat(bean.getClass().getName()).isEqualTo("com.afrozaar.nimbal.test.SpringManagedObject");

            URLClassLoaderExtension extensionClassLoader = (URLClassLoaderExtension) bean.getClass().getClassLoader();

            assertThat(extensionClassLoader.getName()).isEqualTo("nimbal-test-module-annotation");
            assertThat(name).isEqualTo("DefaultConfiguration");
        }
        // unload module
        loader.unloadModule(name);

        loader.loadModule(new MavenCoords("com.afrozaar.nimbal.test", "nimbal-test-module-annotation", "1.1.0-SNAPSHOT"));
        {
            Object bean = registry.getModule(name).getContext().getBean("getBean");

            assertThat(bean).isNotNull();
            String x = ((Supplier<String>) bean).get();
            assertThat(x).isEqualTo("from test simple 2");
        }
    }

    @SuppressWarnings("unchecked")
    @Test
    public void LoadSimpleContextWithSomeCustomisation() throws ErrorLoadingArtifactException, MalformedURLException, IOException, ClassNotFoundException,
            ModuleLoadException {

        BeanPostProcessor mostPostProcessor = mock(BeanPostProcessor.class);

        when(mostPostProcessor.postProcessAfterInitialization(ArgumentMatchers.any(), ArgumentMatchers.anyString())).then(invocation -> invocation.getArgument(
                0));
        when(mostPostProcessor.postProcessBeforeInitialization(ArgumentMatchers.any(), ArgumentMatchers.anyString())).then(invocation -> invocation.getArgument(
                0));

        //Consumer<ConfigurableApplicationContext>.
        ApplicationContext context = loader.loadModule(new MavenCoords("com.afrozaar.nimbal.test", "nimbal-test-simple", "1.0.0-SNAPSHOT"),
                c -> c.getBeanFactory().addBeanPostProcessor(mostPostProcessor)).getContext();

        //LOG.info("beans now present {}", context.getBeanDefinitionNames());
        Object bean = context.getBean("getBean");

        assertThat(bean).isNotNull();
        String x = ((Supplier<String>) bean).get();
        assertThat(x).isEqualTo("from test simple");
        assertThat(bean.getClass().getName()).isEqualTo("com.afrozaar.nimbal.test.SpringManagedObject");

        verify(mostPostProcessor).postProcessBeforeInitialization(ArgumentMatchers.any(), ArgumentMatchers.eq("getBean"));
    }

}
