package com.afrozaar.nimbal.core;

public interface IRegistry {

    Module getModule(String name);

    Module getModule(String groupId, String artifactId);

    Module getModule(MavenCoords mavenCoords2);

    <T> T getBean(final Class<T> clazz);

    <T> T getBean(String beanName);

    Class<?> loadClass(final String name) throws ClassNotFoundException;

}