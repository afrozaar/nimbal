/*******************************************************************************
 * Nimbal Module Manager 
 * Copyright (c) 2017 Afrozaar.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Apache License 2.0
 * which accompanies this distribution and is available at https://www.apache.org/licenses/LICENSE-2.0
 *******************************************************************************/
package com.afrozaar.nimbal.core;

import static java.util.Optional.empty;
import static java.util.Optional.of;

import com.afrozaar.nimbal.api.ModuleLoadException;

import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.context.ConfigurableApplicationContext;

import java.util.function.Function;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Stream;

public class SimpleRegistry implements IRegistry {

    @SuppressWarnings("unused")
    private static final org.slf4j.Logger LOG = org.slf4j.LoggerFactory.getLogger(SimpleRegistry.class);

    Map<String, Module> map = new LinkedHashMap<>();
    Map<Object, Module> mavenCoords = new LinkedHashMap<>();

    public Module registerModule(ModuleInfo moduleInfo, ConfigurableApplicationContext context, ClassLoader classLoader) throws ModuleLoadException {
        Module value = new Module(moduleInfo, classLoader, context);

        if (moduleInfo.parentModule() != null) {
            Module parent = map.get(moduleInfo.parentModule());
            boolean addChild = parent.addChild(value);
            if (addChild) {
                value.setParent(parent);
            } else {
                throw new ModuleLoadException("parent module {} already has this module {} as a child", moduleInfo.parentModule(), moduleInfo.name());
            }
        }
        put(value);
        return value;

    }

    private void put(Module value) {
        mavenCoords.put(value.getMavenCoords().getModuleKey(), value);
        map.put(value.getName(), value);
    }

    /* (non-Javadoc)
     * @see com.afrozaar.nimbal.core.IRegistry#getModule(java.lang.String)
     */
    @Override
    public Module getModule(String name) {
        return map.get(name);
    }

    /* (non-Javadoc)
     * @see com.afrozaar.nimbal.core.IRegistry#getModule(java.lang.String, java.lang.String)
     */
    @Override
    public Module getModule(String groupId, String artifactId) {
        return mavenCoords.get(new MavenCoords(groupId, artifactId, null).getModuleKey());
    }

    public boolean deregister(String moduleName) {
        Module module = map.remove(moduleName);
        if (module == null) {
            return false;
        }
        // remove myself from my parent
        Module parent = module.getParent();
        if (parent != null) {
            parent.removeChild(module);
            module.setParent(null);
        }
        mavenCoords.remove(module.getMavenCoords().getModuleKey());
        return true;
    }

    /* (non-Javadoc)
     * @see com.afrozaar.nimbal.core.IRegistry#getModule(com.afrozaar.nimbal.core.MavenCoords)
     */
    @Override
    public Module getModule(MavenCoords mavenCoords2) {
        return mavenCoords.get(mavenCoords2.getModuleKey());
    }

    public Map<String, Module> getModules() {
        return map;
    }

    @SuppressWarnings("unchecked")
    Function<Module, Optional<Class<?>>> getClass(String className) {
        return module -> {
            try {
                return of(module.getClassLoader().loadClass(className));
            } catch (ClassNotFoundException e) {
                return empty();
            }
        };
    }

    /*
     *     Map<String, Module> map = new LinkedHashMap<>();
     *     Map<Object, Module> mavenCoords = new LinkedHashMap<>();
     */
    @Override
    public <T> T getBean(Class<T> clazz) {
        return find(module -> module.getContext().getBeansOfType(clazz).values().stream().findFirst());
    }

    private <T> T find(Function<Module, Optional<T>> beanFunction) {
        return map.values().stream().map(beanFunction).flatMap(toStream()).findFirst().orElse(null);
    }

    public static <T> Function<Optional<T>, Stream<T>> toStream() {
        return t -> t.map(Stream::of).orElse(Stream.empty());
    }

    @Override
    @SuppressWarnings("unchecked")
    public <T> T getBean(String beanName) {
        return find(module -> {
            try {
                return (Optional<T>) of(module.getContext().getBean(beanName));
            } catch (NoSuchBeanDefinitionException e) {
                return empty();
            }
        });
    }

    @Override
    public Class<?> loadClass(String className) throws ClassNotFoundException {
        try {
            return getClass().getClassLoader().loadClass(className);
        } catch (ClassNotFoundException e) {
            // swallow as we will try other classloaders
        }
        return find(getClass(className));
    }

}
