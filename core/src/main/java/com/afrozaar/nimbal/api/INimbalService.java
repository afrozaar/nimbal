package com.afrozaar.nimbal.api;

import com.afrozaar.nimbal.core.MavenCoords;
import com.afrozaar.nimbal.core.Module;
import com.afrozaar.nimbal.core.SimpleRegistry;

import org.springframework.context.ConfigurableApplicationContext;

import java.util.function.Consumer;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.Map;

public interface INimbalService {

    com.afrozaar.nimbal.core.Module loadModule(MavenCoords mavenCoords, Consumer<ConfigurableApplicationContext>... preRefresh)
            throws ErrorLoadingArtifactException, ModuleLoadException,
            MalformedURLException, IOException,
            ClassNotFoundException;

    void unloadModule(String moduleName) throws ModuleLoadException;

    SimpleRegistry getRegistry();

    com.afrozaar.nimbal.core.Module getModule(String name);

    Map<String, com.afrozaar.nimbal.core.Module> getModules();

}