package com.afrozaar.nimbal.api;

import com.afrozaar.nimbal.core.MavenCoords;

import java.util.List;

public interface INimbalRepository {

    void set(MavenCoords coords, Event status);

    void set(MavenCoords coords, Event status, String message);

    void set(String moduleName, Event unloaded);

    List<String> getActive();

}
