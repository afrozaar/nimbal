/*******************************************************************************
 * Nimbal Module Manager 
 * Copyright (c) 2017 Afrozaar.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Apache License 2.0
 * which accompanies this distribution and is available at https://www.apache.org/licenses/LICENSE-2.0
 *******************************************************************************/
package com.afrozaar.nimbal.test;

import com.afrozaar.nimbal.annotations.IModuleConfigurer;
import com.afrozaar.nimbal.annotations.Module;

import org.springframework.context.annotation.Bean;

@Module
public class DefaultConfiguration implements IModuleConfigurer {

    private String beforeRefresh;
    private String afterRefresh;

    @Bean
    public SpringManagedObject getBean() {
        return new SpringManagedObject();
    }

    @Override
    public void beforeRefresh() {
        this.beforeRefresh = "yes";
    }

    @Override
    public void afterRefresh() {
        this.afterRefresh = "yes";
    }

    public String getBeforeRefresh() {
        return beforeRefresh;
    }

    public void setBeforeRefresh(String beforeRefresh) {
        this.beforeRefresh = beforeRefresh;
    }

    public String getAfterRefresh() {
        return afterRefresh;
    }

}
